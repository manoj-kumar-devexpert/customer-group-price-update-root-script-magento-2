# Customer group price update root script magento 2

## Overview

This script is designed to facilitate the update of customer group prices in an e-commerce system. It provides a convenient way to modify prices for specific customer groups using a root script. This can be particularly useful for bulk updates or periodic adjustments based on various factors.

## Prerequisites

- **Access:** Ensure that you have root access to the server or appropriate permissions to execute scripts.

- **Backup:** Always perform a backup of your database before running any script that modifies data.

- **Environment:** The script assumes a compatible environment with the necessary dependencies installed.

## Usage

1. Dowload code from below link

https://gitlab.com/manoj-kumar-devexpert/customer-group-price-update-root-script-magento-2/-/tree/main

2. Replace website id and configurable product id.

3. Run as root script like command below

php update-tierprice.php
