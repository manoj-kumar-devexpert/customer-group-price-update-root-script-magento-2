<?php
use Magento\Catalog\Api\ProductRepositoryInterface;
use Magento\Framework\App\Bootstrap;
require __DIR__ . "/app/bootstrap.php";
$bootstrap = Bootstrap::create(BP, $_SERVER);
$objectManager = $bootstrap->getObjectManager();
$appState = $objectManager->get("\Magento\Framework\App\State");
$appState->setAreaCode("adminhtml");
$productRepository = $objectManager->get(ProductRepositoryInterface::class);
$sku = "WJ5201SL"; //configurable product id
$product = $productRepository->get($sku);

if ($product->getTypeId() == \Magento\ConfigurableProduct\Model\Product\Type\Configurable::TYPE_CODE)
{
    $childProducts = $product->getTypeInstance()
        ->getUsedProducts($product);

    foreach ($childProducts as $childProduct)
    {
        $productId = $childProduct->getId();
        $websiteIds = $childProduct->getWebsiteIds();
        $websiteIdscount = count($websiteIds);
        if ($websiteIdscount == 1 && in_array("7", $websiteIds))
        {
            echo "website 7";
            echo $productId;


            $tierPrices = [["website_id" => 7, "cust_group" => 32000, "price_qty" => 1, "price" => 18.0, ], ["website_id" => 7, "cust_group" => 32000, "price_qty" => 5, "price" => 16.0, ], ["website_id" => 7, "cust_group" => 32000, "price_qty" => 10, "price" => 15.0, ], ["website_id" => 7, "cust_group" => 32000, "price_qty" => 15, "price" => 14.0, ], ];
        }
        if ($websiteIdscount == 1 && in_array("10", $websiteIds))
        {
            echo "website 10";
            echo $productId;
   
            $tierPrices = [["website_id" => 10, "cust_group" => 32000, "price_qty" => 1, "price" => 22.0, ], ["website_id" => 10, "cust_group" => 32000, "price_qty" => 5, "price" => 18.0, ], ["website_id" => 10, "cust_group" => 32000, "price_qty" => 10, "price" => 16.0, ], ["website_id" => 10, "cust_group" => 32000, "price_qty" => 15, "price" => 15.0, ], ];
        }
        if ($websiteIdscount == 3 && in_array("7", $websiteIds) && in_array("9", $websiteIds) && in_array("10", $websiteIds))
        {
            echo "website 7,9,10";
            echo $productId;
 
            $tierPrices = [["website_id" => 7, "cust_group" => 32000, "price_qty" => 1, "price" => 18.0, ], ["website_id" => 9, "cust_group" => 32000, "price_qty" => 1, "price" => 20.0, ], ["website_id" => 7, "cust_group" => 32000, "price_qty" => 5, "price" => 16.0, ], ["website_id" => 9, "cust_group" => 32000, "price_qty" => 5, "price" => 16.0, ], ["website_id" => 7, "cust_group" => 32000, "price_qty" => 10, "price" => 15.0, ], ["website_id" => 9, "cust_group" => 32000, "price_qty" => 10, "price" => 14.0, ], ["website_id" => 7, "cust_group" => 32000, "price_qty" => 15, "price" => 14.0, ], ["website_id" => 9, "cust_group" => 32000, "price_qty" => 15, "price" => 12.0, ], ["website_id" => 10, "cust_group" => 32000, "price_qty" => 1, "price" => 22.0, ], ["website_id" => 10, "cust_group" => 32000, "price_qty" => 5, "price" => 18.0, ], ["website_id" => 10, "cust_group" => 32000, "price_qty" => 10, "price" => 16.0, ], ["website_id" => 10, "cust_group" => 32000, "price_qty" => 15, "price" => 15.0, ], ];
        }

        if ($websiteIdscount == 1 && in_array("9", $websiteIds))
        {
            echo "website 9";
            echo $productId;

            $tierPrices = [["website_id" => 9, "cust_group" => 32000, "price_qty" => 1, "price" => 20.0, ], ["website_id" => 9, "cust_group" => 32000, "price_qty" => 5, "price" => 16.0, ], ["website_id" => 9, "cust_group" => 32000, "price_qty" => 10, "price" => 14.0, ], ["website_id" => 9, "cust_group" => 32000, "price_qty" => 15, "price" => 12.0, ], ];
        }

        if ($websiteIdscount == 2 && in_array("7", $websiteIds) && in_array("10", $websiteIds))
        {
            echo "website 7,10";
            echo $productId;

            $tierPrices = [["website_id" => 7, "cust_group" => 32000, "price_qty" => 1, "price" => 18.0, ], ["website_id" => 10, "cust_group" => 32000, "price_qty" => 1, "price" => 22.0, ], ["website_id" => 7, "cust_group" => 32000, "price_qty" => 5, "price" => 16.0, ], ["website_id" => 10, "cust_group" => 32000, "price_qty" => 5, "price" => 18.0, ], ["website_id" => 7, "cust_group" => 32000, "price_qty" => 10, "price" => 15.0, ], ["website_id" => 10, "cust_group" => 32000, "price_qty" => 10, "price" => 16.0, ], ["website_id" => 7, "cust_group" => 32000, "price_qty" => 15, "price" => 14.0, ], ["website_id" => 10, "cust_group" => 32000, "price_qty" => 15, "price" => 15.0, ], ];
        }
        if ($websiteIdscount == 2 && in_array("7", $websiteIds) && in_array("9", $websiteIds))
        {
            echo "website 7,9";
            echo $productId;

            $tierPrices = [["website_id" => 7, "cust_group" => 32000, "price_qty" => 1, "price" => 18.0, ], ["website_id" => 9, "cust_group" => 32000, "price_qty" => 1, "price" => 20.0, ], ["website_id" => 7, "cust_group" => 32000, "price_qty" => 5, "price" => 16.0, ], ["website_id" => 9, "cust_group" => 32000, "price_qty" => 5, "price" => 16.0, ], ["website_id" => 7, "cust_group" => 32000, "price_qty" => 10, "price" => 15.0, ], ["website_id" => 9, "cust_group" => 32000, "price_qty" => 10, "price" => 14.0, ], ["website_id" => 7, "cust_group" => 32000, "price_qty" => 15, "price" => 14.0, ], ["website_id" => 9, "cust_group" => 32000, "price_qty" => 15, "price" => 12.0, ], ];
        }

        $tierPrice = $childProduct->getTierPrice();
        if (empty($tierPrice))
        {
            $childProduct->setTierPrice($tierPrices);
            $childProduct->save();

            echo $childProduct->getSku();
            echo "<br>";

        }
    }

    echo "done";
}

